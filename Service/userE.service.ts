import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserDto} from '../model/user.dto';
import {DepartmentEnum} from '../model/enumeration/department.enum';
import {RoleEnum} from "../model/enumeration/role.enum";

@Injectable({
  providedIn: 'root'
})
export class UserEService {

  baseUrl: string = 'http://localhost:8080/api';

  constructor(private httpClient: HttpClient) {
  }

  getUsers(): Observable<UserDto[]> {
    return this.httpClient.get<UserDto[]>(this.baseUrl + "/user/all");

  }

  getUser(id?: number): Observable<UserDto> {
    return this.httpClient.get(this.baseUrl + "/user/find/" + id);
  }

  updateUser(user?: UserDto): Observable<any> {
    return this.httpClient.post<any>(this.baseUrl + "/user/update", user);
  }

  getDepartmentList(): Observable<DepartmentEnum[]> {
    return this.httpClient.get<DepartmentEnum[]>(this.baseUrl + "/user/department/all");
  }

  addUser(user?: UserDto): Observable<any> {
    return this.httpClient.post<any>(this.baseUrl + "/user/add", user);
  }

  getRoles(): Observable<RoleEnum[]> {
    return this.httpClient.get<RoleEnum[]>(this.baseUrl + "/user/role/all")
  }

  deleteUser(id?: number): Observable<any> {
    return this.httpClient.get(this.baseUrl + "/user/delete/" + id);
  }

  putNewPassword(user?: UserDto): Observable<any> {
    // @ts-ignore
    return this.httpClient.post<any>(this.baseUrl + "/user/updatePassword", user);
  }

}

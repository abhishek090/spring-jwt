import {Component, OnInit} from '@angular/core';
import {AuthService} from "../_services/auth.service";
import {StorageService} from "../_services/storage.service";
import {Router} from "@angular/router";
import {GlobalEventManager} from "../global-event-manager";
import {MessageService} from "primeng/api";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: any = {
    username: null,
    password: null,
  };
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];

  constructor(private authService: AuthService,
              private router: Router,
              private storageService: StorageService,
              private globalEventManager: GlobalEventManager,
              private messageService: MessageService) {
  }

  ngOnInit(): void {

    if (this.storageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;

    }
  }

  onSubmit(): void {
    const {username, password} = this.form;

    this.authService.login(username, password).subscribe({
      next: data => {
        this.storageService.saveUser(data);
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.storageService.getUser().roles;
        this.globalEventManager.showNavBar.emit(true);
        this.navigateToDashboard();
      },

      error: err => {
        console.log(JSON.stringify(err));
        this.wrongPasswordToast();
      }
    });
  }

  private navigateToDashboard() {
    this.router.navigate(['user']);
  }

  wrongPasswordToast() {
    this.messageService.add({
      severity: 'error',
      summary: 'Login failed !',
      detail: 'Please check your credentials and try again ! '
    });
  }
}

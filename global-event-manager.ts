import {EventEmitter, Injectable} from "@angular/core";

@Injectable()
export class GlobalEventManager {
  public showNavBar: EventEmitter<any> = new EventEmitter<any>();
}

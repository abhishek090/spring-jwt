import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

const testUrl = 'http://localhost:8080/api/test/';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  constructor(private http: HttpClient) {
  }

  getPublicContent(): Observable<any> {
    return this.http.get(testUrl + 'all', {responseType: 'text'});
  }

  getUserBoard(): Observable<any> {
    return this.http.get(testUrl + 'user', {responseType: 'text'});
  }

  getModeratorBoard(): Observable<any> {
    return this.http.get(testUrl + 'mod', {responseType: 'text'});
  }

  getAdminBoard(): Observable<any> {
    return this.http.get(testUrl + 'admin', {responseType: 'text'});
  }
}

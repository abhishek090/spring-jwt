import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {MenuItem, MessageService} from "primeng/api";
import {StorageService} from "../_services/storage.service";
import {AuthService} from "../_services/auth.service";
import {Router} from "@angular/router";
import {GlobalEventManager} from "../global-event-manager";
import {UserEService} from "../Service/userE.service";
import {UserDto} from "../model/user.dto";

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit, OnChanges {
  isLoggedIn?: boolean;
  items?: MenuItem[] = [];
  username?: string;
  showNavBar?: boolean;
  passwordDialog?: boolean;
  user?: UserDto;
  confirmPassword?: string;


  constructor(private storageService: StorageService,
              private authService: AuthService,
              private router: Router,
              private globalEventManager: GlobalEventManager,
              private userService: UserEService,
              private messageService : MessageService) {
  }

  ngOnInit() {
    this.globalEventManager.showNavBar.subscribe((mode: boolean) => {
      this.showNavBar = mode;
      if (this.showNavBar) {
        const user = this.storageService.getUser();
        this.username = user.firstName + ' ' + user.lastName;
      }
    });
    this.isLogIn();
  }

  private isLogIn() {
    this.isLoggedIn = this.storageService.isLoggedIn();
    if (this.isLoggedIn) {
      this.globalEventManager.showNavBar.emit(true);
    }
  }

  logout(): void {
    this.authService.logout().subscribe({
      next: res => {
        this.storageService.clean();
        this.globalEventManager.showNavBar.emit(false);
        this.navigateToLogin();
      },
      error: err => {
        console.log(err);
      }
    });
  }

  private navigateToLogin() {
    this.router.navigate(['login']);
  }


  ngOnChanges(changes: SimpleChanges): void {
    this.isLogIn();
  }

  updatePassword() {
    this.user = this.storageService.getUser();
    this.passwordDialog = true;
  }

  putPassword() {
    this.userService.putNewPassword(this.user).subscribe(resp => {
      this.passwordDialog = false;
      this.logout();
      this.navigateToLogin();
      console.log("password updated");
      this.passwordToast();
    })
  }

  closeDialog() {
    this.passwordDialog = false;
  }

  passwordToast() {
    this.messageService.add({severity:'success',  detail:'Password updated successfully, Login again ! '});
  }


}


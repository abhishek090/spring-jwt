import {NgModule} from '@angular/core';

import {UserComponent} from './user.component';
import {TableModule} from 'primeng/table';
import {UserDetailComponent} from './user-detail/user-detail.component';
import {CommonModule} from '@angular/common';
import {UserRoutingModule} from './user.routing.module';
import {UserHomeComponent} from './user-home/user-home.component';
import {DialogModule} from 'primeng/dialog';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {CardModule} from 'primeng/card';
import {FormsModule} from '@angular/forms';
import {DropdownModule} from 'primeng/dropdown';
import {UserDialogComponent} from './user-dialog/user-dialog.component';
import {ToolbarModule} from 'primeng/toolbar';
import {InputNumberModule} from "primeng/inputnumber";
import {MenubarModule} from "primeng/menubar";
import {MultiSelectModule} from 'primeng/multiselect';
import {ToastModule} from "primeng/toast";
import {MessageService} from "primeng/api";


@NgModule({
  declarations: [
    UserComponent,
    UserDetailComponent,
    UserHomeComponent,
    UserDialogComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    TableModule,
    DialogModule,
    ButtonModule,
    InputTextModule,
    CardModule,
    FormsModule,
    DropdownModule,
    ToolbarModule,
    InputNumberModule,
    MenubarModule,
    MultiSelectModule,
    ToastModule


  ],
  providers : [MessageService],
})

export class UserModule {
}

import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserDto} from '../../model/user.dto';
import {UserEService} from '../../Service/userE.service';


@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  @Output() parentFunction: EventEmitter<any> = new EventEmitter<any>();

  user?: UserDto;
  id?: number;
  display?: boolean;

  constructor(private userService: UserEService,
              private route: ActivatedRoute,
  ) {
  }

  showDialog() {
    this.display = true;
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.getUser();
    this.parentFunction.emit(this.user);
  }

  getUser() {
    this.userService.getUser(this.id).subscribe(user => {
      this.user = user;
      this.display = false;
    });
  }


}

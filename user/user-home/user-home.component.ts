import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserDto} from 'src/app/model/user.dto';
import {UserEService} from 'src/app/Service/userE.service';
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.css']
})
export class UserHomeComponent implements OnInit {

  users: UserDto[] = [];
  user?: UserDto;

  display: boolean = false;


  constructor(private userService: UserEService,
              private router: Router,
              private route: ActivatedRoute,
              private messageService : MessageService
             ) {

  }

  ngOnInit() {
    this.getUsers();
  }

  onRowSelect(selectedUser: UserDto) {
    this.router.navigate([`user-detail/${selectedUser.id}`], {relativeTo: this.route});
  }

  showDialog() {
    this.user = new UserDto();
    this.display = true;
  }

  getUsers() {
    this.userService.getUsers().subscribe(users => {
      this.users = users;
      this.display=false;
    })
  }

  deleteUser(user: UserDto) {
    this.userService.deleteUser(user.id).subscribe(result => {
      this.getUsers();
      this.deleteSingle();
    })
  }

  deleteSingle() {
    this.messageService.add({severity:'error', summary:'Deleted', detail:'User deleted successfully.'});
  }


}


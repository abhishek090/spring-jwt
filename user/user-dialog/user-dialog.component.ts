import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DropdownDto} from 'src/app/model/dropdown.dto';
import {DepartmentEnum} from 'src/app/model/enumeration/department.enum';
import {UserDto} from 'src/app/model/user.dto';
import {UserEService} from 'src/app/Service/userE.service';
import {RoleEnum} from "../../model/enumeration/role.enum";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.css']
})
export class UserDialogComponent implements OnInit {

  @Input() user?: UserDto;
  @Input() display?: boolean;
  departmentDropdownList?: DropdownDto[] = [];
  departmentList?: DepartmentEnum[];
  roleDropDownList?: DropdownDto[] = [];
  roleList?: RoleEnum[];

  oneDepartment?: DepartmentEnum | undefined;
  selectedRoles?: RoleEnum[];

  @Output() refreshUser = new EventEmitter<boolean>()


  constructor(private userService: UserEService,
              private messageService : MessageService) {
  }

  ngOnInit(): void {
    this.getDepartmentList();
    this.getRolesList();
  }

  updateUser() {
    // @ts-ignore
    this.user.department = this.oneDepartment;
    // @ts-ignore
    this.user.userRoles = this.selectedRoles;
    this.userService.updateUser(this.user).subscribe(resp => {
      this.refreshUser.emit(true);
    });
  }

  addUser() {
    this.userService.addUser(this.user).subscribe(resp => {

      this.refreshUser.emit(true);

    });
  }

  saveUser() {
    if (this.user?.id) {
      this.updateUser();
      this.updateToast();
    } else {
      this.addUser();
      this.addToast();
    }
  }

  getDepartmentList() {
    this.userService.getDepartmentList().subscribe(response => {

      this.departmentList = response;
      this.departmentDropdownList?.push({value: null, label: '--Select department--'});
      this.departmentList.forEach(department => {
        this.departmentDropdownList?.push({value: department, label: department.toString()});
        this.oneDepartment = this.user?.department;
      })
    });
  }

  getRolesList() {
    this.userService.getRoles().subscribe(response => {
      this.roleList = response;
      this.roleList.forEach(role => {
        this.roleDropDownList?.push({value: role, label: role.toString()});
        this.selectedRoles=this.user?.userRoles;
      });
    });
  }

  closeDialog() {
    this.refreshUser.emit(true);
  }

  updateToast() {
    this.messageService.add({severity:'info', summary:'Updated', detail:'User info updated successfully.'});
  }

  addToast() {
    this.messageService.add({severity:'Success', summary:'Added', detail:'User Added successfully.'});
  }
}

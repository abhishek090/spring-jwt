import {DepartmentEnum} from "./enumeration/department.enum";
import {RoleEnum} from "./enumeration/role.enum";

export class UserDto {
  id?: number;
  username?: string;
  firstName?: string;
  lastName?: string;
  phoneNo?: string;
  email?: string;
  salary?: number;
  password?: string;
  department?: DepartmentEnum;
  userRoles?: RoleEnum[] = [];
}
